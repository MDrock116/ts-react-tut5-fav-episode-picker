import React from 'react';
import {Link} from '@reach/router'
import {Store} from "./Store";

export function App(props:any):JSX.Element {
    const {state} = React.useContext(Store)
    return (
      <React.Fragment>
          <header className="header">
            <h1>Rick and Morty</h1>
            <p>Pick your favourite episode!!!</p>
              <Link to='/'>Home</Link>
              <Link to='/faves'>Favorite(s): {state.favourites.length}</Link>
          </header>
          {props.children}
      </React.Fragment>
  )
}


