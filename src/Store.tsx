import React from 'react'
import {IAction, IState} from "./interfaces";

const intialState:IState = {
    episodes: [],
    favourites: []
}

export const Store = React.createContext<IState | any>(intialState) //'any' allows 'Store.Provider' to be value from reducer

function reducer(state:IState, action:IAction): IState{
    switch (action.type) {
        case 'FETCH_DATA': return {...state, episodes: action.payload}  //State isn't changed here, it's a copy, it's only changed through the reducer.
        case 'ADD_FAV': return {...state, favourites: [ ...state.favourites, action.payload] }  //we add everything that was in the state first but we need to add what's in the favourites again or we'll overwrite it w/ the payload
        case 'REMOVE_FAV': return {...state, favourites: action.payload }
        default: return state

    }
}

export function StoreProvider(props: Element | any): JSX.Element{  //props holds list of child components
    const [state, dispatch] = React.useReducer(reducer, intialState)
    return <Store.Provider value={{state, dispatch}}>{props.children}</Store.Provider>  //wrap children w/ access to context variable(i.e. reducer)
}