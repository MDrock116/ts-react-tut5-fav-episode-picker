import React from 'react';
import {IEpisodeProps} from "./interfaces";
import {Store} from "./Store";
import {fetchDataAction, toggleFavAction} from "./Actions";
const EpisodeList = React.lazy<any>(() => import('./EpisodesList'))  //importing lazily

export default function HomePage(): JSX.Element {
    const {state, dispatch} = React.useContext(Store)

    React.useEffect(() =>{
        state.episodes.length === 0 && fetchDataAction(dispatch)  //runs Action if length=0
    })  //I think this only runs at onMount because nothing changes since there is no last param

    const props: IEpisodeProps = {
        episodes: state.episodes,
        store: {state, dispatch},
        toggleFavAction,  //in ES6 can skip assignment variable name if it's the same, also no arguments are passed because just a function is being passed
        favourites: state.favourites
    }

    return(
        <React.Fragment>
            <React.Suspense fallback={<div>loading...</div> }>
                <section  className="episode-layout">
                    <EpisodeList {...props} /> {/*spread props values into props w/ ES6 skip assignment var*/}
                </section>
            </React.Suspense>
        </React.Fragment>
    )
}