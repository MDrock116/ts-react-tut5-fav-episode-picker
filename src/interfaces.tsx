import React from "react";

export type Dispatch = React.Dispatch<IAction>

export interface IState {
    episodes: Array<IEpisode>,
    favourites: Array<IEpisode>
}

export interface IAction {
    type: string,
    payload: Array<IEpisode> | any  //any is needed here because javascript isn't smart to understand that the complex favourites assignment in the 'ADD_FAV' reducer code is a Array<IEpisode>
}

export interface IEpisode {
    airdate: string
    airstamp: string
    airtime: string
    id: number
    image: {medium: string, original: string}
    name: string
    number: number
    runtime: number
    season: number
    summary: string
    url: string
}

export interface IEpisodeProps {
    episodes: Array<IEpisode>,
    store: {state:IState, dispatch:any}
    toggleFavAction: (state:IState, dispatch:Dispatch, episode: IEpisode) => IAction,
    favourites: Array<IEpisode>
}