import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {App} from './App';
import {StoreProvider} from './Store';
import {RouteComponentProps, Router} from "@reach/router";
import HomePage from "./HomePage";
import FavPage from "./FavPage";

const  RouterPage = (props: {pageComponent: JSX.Element} & RouteComponentProps) => props.pageComponent
ReactDOM.render(
  <StoreProvider>
      <Router>
          <App path={'/'}>
              {/*the following are injected as {props.children} in App.tsx code*/}
              {/*the Homepage is initially shown because the path assigned to it below matches the prop sent in on the App above*/}
              <RouterPage pageComponent={<HomePage/>} path='/' />
              <RouterPage pageComponent={<FavPage/>} path='/faves' />
          </App>
      </Router>
  </StoreProvider>,
  document.getElementById('root')
);

