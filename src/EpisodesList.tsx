import React from 'react';
import {IEpisode} from "./interfaces";

function EpisodesList(props:any): Array<JSX.Element> {
    const {episodes, store, toggleFavAction, favourites} = props
    const {state, dispatch} = store
    return episodes.map((episode:IEpisode) => {
        return(
            <section key={episode.id} className="episode-bx">
                <img src={episode.image?.medium} alt={`Rick and Mort ${episode.name}`} />
                <div>{episode.name}</div>
                <section style={{display: 'flex', justifyContent: 'space-between'}}>
                    <div>Season" {episode.season} Number: {episode.number}</div>
                    <button type="button" onClick={() => toggleFavAction(state, dispatch, episode)}>
                        {" "}
                        {/*{state.favourites.includes(episode) ? "UnFav" : "Fav"}{" "}*/}
                        {favourites.find(
                            (fav:IEpisode) => fav.id===episode.id) ? "UnFav" : "Fav"}
                    </button>
                </section>
            </section>
        )
    })
}

export default EpisodesList;