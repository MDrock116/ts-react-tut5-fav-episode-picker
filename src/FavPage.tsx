import React from 'react';
import {Store} from "./Store";
import {IEpisodeProps} from "./interfaces";
import {toggleFavAction} from "./Actions";
const EpisodeList = React.lazy<any>(() => import('./EpisodesList'))  //importing lazily

export default function FavPage(): JSX.Element {
    const {state, dispatch} = React.useContext(Store)

    const props: IEpisodeProps = {
        episodes: state.favourites,
        store: {state, dispatch},
        toggleFavAction,  //in ES6 can skip assignment variable name if it's the same, also no arguments are passed because just a function is being passed
        favourites: state.favourites
    }

    return (
        <React.Suspense fallback={<div>loading...</div> }>
            <div className="episode-layout">
                <EpisodeList {... props} />
            </div>
        </React.Suspense>

    );
}