import {IAction, IEpisode, IState} from "./interfaces";

export const fetchDataAction = async (dispatch: any) => {
    const URL = 'https://api.tvmaze.com/singlesearch/shows?q=rick-&-morty&embed=episodes'
    const data = await fetch(URL)
    const dataJSON = await data.json();
    return dispatch({
        type: 'FETCH_DATA',
        payload: dataJSON._embedded.episodes
    })
}

//Wraps dispatch w/ function containing episode param which its implementation sees through closure
//dispatch itself, takes an Action param & returns an Action from reducer, reducer also returns a separate variable called 'state'
export const toggleFavAction = (state:IState, dispatch:any, episode: IEpisode):IAction  => {
    const episodeInFav: boolean = state.favourites.includes(episode)
    let action:IAction = {  //dispatch
        type: 'ADD_FAV',
        payload: episode
    }
    if(episodeInFav){
        let favsWithoutEpisode: IEpisode[] = state.favourites.filter(
            (fav:IEpisode)=> episode!==fav)
        action = {
            type: 'REMOVE_FAV',
            payload: favsWithoutEpisode
        }
    }
    return dispatch(action)
}